import XCTest
@testable import Traffic_Lights

class IntersectionInteractorTests: XCTestCase {

    func test_stop_preventsFutherChanges() {
        let testExpectation = expectation(description: "")
        let interactor = IntersectionInteractor(switchInterval: 0.5, waitInterval: 0.1)
        
        var switches = 0
        interactor.start { _ in
            switches += 1
            _ = interactor.stop()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            XCTAssertEqual(switches, 1)
            testExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func test_stop_resetsToCautionAll() {
        let interactor = IntersectionInteractor(switchInterval: 0.5, waitInterval: 0.1)
        
        interactor.start { _ in }
        let intersection = interactor.stop()
        
        XCTAssertEqual(intersection, .cautionAll)
    }
    
    func test_start_immediatelyGoNorthSouth() {
        let testExpectation = expectation(description: "")
        let interactor = IntersectionInteractor(switchInterval: 30, waitInterval: 5)
        
        interactor.start { intersection in
            _ = interactor.stop()
            
            XCTAssertEqual(intersection, .goNorthSouth)
            testExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func test_start_fullCycleOfChanges() {
        let interactor = IntersectionInteractor(switchInterval: 1, waitInterval: 0.25)

        let expectedSwitches: [(time: TimeInterval, intersection: Intersection)] = [
            (time: 0, intersection: .goNorthSouth),
            (time: 0.75, intersection: .waitNorthSouth),
            (time: 1, intersection: .goEastWest),
            (time: 1.75, intersection: .waitEastWest),
            (time: 2, intersection: .goNorthSouth)
        ]
        
        assertSwitches(of: interactor, expected: expectedSwitches, timeout: 3)
    }
    
    func test_start_zeroWaitInterval() {
        let interactor = IntersectionInteractor(switchInterval: 0.25, waitInterval: 0)
        
        let expectedSwitches: [(time: TimeInterval, intersection: Intersection)] = [
            (time: 0, intersection: .goNorthSouth),
            (time: 0.25, intersection: .goEastWest),
            (time: 0.5, intersection: .goNorthSouth)
        ]
        
        assertSwitches(of: interactor, expected: expectedSwitches, timeout: 1)
    }
    
    func test_start_ignoresWaitIntervalLargerThanSwitchInterval() {
        let interactor = IntersectionInteractor(switchInterval: 0.25, waitInterval: 0.5)
        
        let expectedSwitches: [(time: TimeInterval, intersection: Intersection)] = [
            (time: 0, intersection: .goNorthSouth),
            (time: 0.25, intersection: .goEastWest),
            (time: 0.5, intersection: .goNorthSouth)
        ]
        
        assertSwitches(of: interactor, expected: expectedSwitches, timeout: 1)
    }
    
    func test_start_ignoresWaitIntervalEqualToSwitchInterval() {
        let interactor = IntersectionInteractor(switchInterval: 0.25, waitInterval: 0.25)
        
        let expectedSwitches: [(time: TimeInterval, intersection: Intersection)] = [
            (time: 0, intersection: .goNorthSouth),
            (time: 0.25, intersection: .goEastWest),
            (time: 0.5, intersection: .goNorthSouth)
        ]
        
        assertSwitches(of: interactor, expected: expectedSwitches, timeout: 1)
    }
    
    func test_start_zeroSwitchInterval() {
        let testExpectation = expectation(description: "")
        let interactor = IntersectionInteractor(switchInterval: 0, waitInterval: 0.25)
        
        var switches = 0
        interactor.start { _ in
            switches += 1
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            XCTAssertEqual(switches, 1)
            _ = interactor.stop()
            testExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
    }
}

private extension XCTestCase {
    
    func assertSwitches(of interactor: IntersectionInteractorProtocol, expected expectedSwitches: [(time: TimeInterval, intersection: Intersection)], timeout: TimeInterval) {
        let testExpectation = expectation(description: "")
        
        var actualSwitches = [(time: TimeInterval, intersection: Intersection)]()
        let startTime = CACurrentMediaTime()
        
        interactor.start { intersection in
            let changeTime = CACurrentMediaTime() - startTime
            print(String(format: "Switch at %.2f", changeTime))
            actualSwitches.append((time: changeTime, intersection: intersection))
            
            guard actualSwitches.count == expectedSwitches.count else {
                return
            }
            
            _ = interactor.stop()
            
            for (actual, expected) in zip(actualSwitches, expectedSwitches) {
                XCTAssertEqualWithAccuracy(actual.time, expected.time, accuracy: 0.2)
                XCTAssertEqual(actual.intersection, expected.intersection)
            }
            
            testExpectation.fulfill()
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
}
