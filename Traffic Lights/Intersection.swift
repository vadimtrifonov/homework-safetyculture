import Foundation

enum TrafficLight {
    case red
    case yellow
    case green
}

struct Intersection: Equatable {
    
    static let goNorthSouth = Intersection(north: .green, east: .red, south: .green, west: .red)
    static let goEastWest = Intersection(north: .red, east: .green, south: .red, west: .green)
    static let waitNorthSouth = Intersection(north: .yellow, east: .red, south: .yellow, west: .red)
    static let waitEastWest = Intersection(north: .red, east: .yellow, south: .red, west: .yellow)
    static let cautionAll = Intersection()
    
    let north: TrafficLight
    let east: TrafficLight
    let south: TrafficLight
    let west: TrafficLight
    
    private init(north: TrafficLight = .yellow, east: TrafficLight = .yellow, south: TrafficLight = .yellow, west: TrafficLight = .yellow) {
        self.north = north
        self.east = east
        self.south = south
        self.west = west
    }

    func switched() -> Intersection {
        switch self {
        case Intersection.goNorthSouth, Intersection.waitNorthSouth:
            return .goEastWest
        case Intersection.goEastWest, Intersection.waitEastWest:
            return .goNorthSouth
        case Intersection.cautionAll:
            return .goNorthSouth
        default:
            return .cautionAll
        }
    }
    
    func waiting() -> Intersection {
        switch self {
        case Intersection.goNorthSouth, Intersection.waitNorthSouth:
            return .waitNorthSouth
        case Intersection.goEastWest, Intersection.waitEastWest:
            return .waitEastWest
        default:
            return .cautionAll
        }
    }
    
    static func ==(lhs: Intersection, rhs: Intersection) -> Bool {
        return lhs.north == rhs.north
            && lhs.east == rhs.east
            && lhs.south == rhs.south
            && lhs.west == rhs.west
    }
}
