import Foundation

extension Timer {
    
    class func scheduledTimer(_ fireInterval: TimeInterval, repeat repeatInterval: TimeInterval = 0, handler: @escaping () -> Void) -> Timer {
        let timer = CFRunLoopTimerCreateWithHandler(kCFAllocatorDefault, CFAbsoluteTimeGetCurrent() + fireInterval, repeatInterval, 0, 0) { _ in
            handler()
        }!
        RunLoop.current.add(timer, forMode: .defaultRunLoopMode)
        return timer
    }
}
