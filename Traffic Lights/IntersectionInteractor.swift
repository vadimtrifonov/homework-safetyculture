import Foundation

protocol IntersectionInteractorProtocol {
    typealias SwitchHandler = (Intersection) -> Void
    
    var isRunning: Bool { get }
    
    init(switchInterval: TimeInterval, waitInterval: TimeInterval)
    
    func start(switchHandler: @escaping SwitchHandler)
    func stop() -> Intersection
}

final class IntersectionInteractor: IntersectionInteractorProtocol {
    
    var isRunning: Bool {
        return switchTimer.isValid || waitTimer.isValid
    }
    
    private let switchInterval: TimeInterval
    private let waitInterval: TimeInterval
    
    private lazy var switchTimer = Timer()
    private lazy var waitTimer = Timer()
    
    private(set) var intersection: Intersection = .cautionAll
    
    init(switchInterval: TimeInterval, waitInterval: TimeInterval) {
        self.switchInterval = switchInterval
        self.waitInterval = switchInterval > waitInterval ? waitInterval : 0
    }
    
    func start(switchHandler: @escaping IntersectionInteractorProtocol.SwitchHandler) {
        guard !switchTimer.isValid && !waitTimer.isValid else {
            return
        }

        switchTimer = Timer.scheduledTimer(0, repeat: switchInterval) { [weak self] in
            guard let _self = self else {
                return
            }
            
            _self.intersection = _self.intersection.switched()
            switchHandler(_self.intersection)
        }
        
        guard waitInterval != 0 else {
            return
        }
        
        waitTimer = Timer.scheduledTimer(switchInterval - waitInterval, repeat: switchInterval) { [weak self] in
            guard let _self = self else {
                return
            }

            _self.intersection = _self.intersection.waiting()
            switchHandler(_self.intersection)
        }
    }
    
    func stop() -> Intersection {
        switchTimer.invalidate()
        waitTimer.invalidate()
        
        intersection = .cautionAll
        return intersection
    }
}
