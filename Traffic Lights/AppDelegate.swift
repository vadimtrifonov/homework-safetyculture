import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        guard let viewController = window?.rootViewController as? IntersectionInteractorDepending else {
            fatalError("Unexpected view controller type")
        }
        viewController.interactor = IntersectionInteractor(switchInterval: 30, waitInterval: 5)
        
        return true
    }
}

