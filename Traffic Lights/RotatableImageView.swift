import UIKit

@IBDesignable
final class RotatableImageView: UIImageView {
    
    @IBInspectable
    var degrees: CGFloat = 0 {
        didSet {
            super.image = super.image?.rotated(by: degrees)
        }
    }
    
    override var image: UIImage? {
        get {
            return super.image
        }
        set {
            super.image = newValue?.rotated(by: degrees)
        }
    }
}

extension UIImage {

    func rotated(by degrees: CGFloat) -> UIImage? {
        guard let cgImage = cgImage else {
            return nil
        }
        
        let radians = degrees * CGFloat.pi / 180
        let transform = CGAffineTransform(rotationAngle: radians)
        let rotatedSize = CGRect(origin: .zero, size: size).applying(transform).size
        
        UIGraphicsBeginImageContext(rotatedSize)
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        context?.rotate(by: radians)
        context?.scaleBy(x: 1, y: -1)
        
        let drawRect = CGRect(origin: CGPoint(x: -size.width / 2, y: -size.height / 2), size: size)
        context?.draw(cgImage, in: drawRect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
