import UIKit

protocol IntersectionInteractorDepending: class {
    
    var interactor: IntersectionInteractorProtocol! { get set }
}

class IntersectionViewController: UIViewController, IntersectionInteractorDepending {

    var interactor: IntersectionInteractorProtocol!
    
    @IBOutlet private weak var northTrafficLight: UIImageView!
    @IBOutlet private weak var eastTrafficLight: UIImageView!
    @IBOutlet private weak var southTrafficLight: UIImageView!
    @IBOutlet private weak var westTrafficLight: UIImageView!
    @IBOutlet weak var toggleAnimationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toggleAnimationButton.setTitle(NSLocalizedString("Animate", comment: ""), for: [])
    }
    
    @IBAction func toggleAnimation(_ sender: Any) {
        let title = interactor.isRunning ? NSLocalizedString("Animate", comment: "") : NSLocalizedString("Stop", comment: "")
        toggleAnimationButton.setTitle(title, for: [])
        
        if interactor.isRunning {
            let intersection = interactor.stop()
            render(intersection: intersection)
        }
        else {
            interactor.start { [weak self] intersection in
                self?.render(intersection: intersection)
            }
        }
    }
    
    private func render(intersection: Intersection) {
        northTrafficLight.image = intersection.north.image
        eastTrafficLight.image = intersection.east.image
        southTrafficLight.image = intersection.south.image
        westTrafficLight.image = intersection.west.image
    }
}

extension TrafficLight {
    
    var image: UIImage {
        switch self {
        case .green:
            return #imageLiteral(resourceName: "green")
        case .yellow:
            return #imageLiteral(resourceName: "amber")
        case .red:
            return #imageLiteral(resourceName: "red")
        }
    }
}
